package main

import (
	"gitlab.com/cherrug/go-calendar/app/delivery/rest"
	"gitlab.com/cherrug/go-calendar/config"
	"gitlab.com/cherrug/go-calendar/logger"
)

func main() {
	cfg := config.New()
	lg := logger.New(cfg)
	defer lg.Zap.Sync()

	rest.Run(cfg.Host, cfg.Port, &lg)

}
