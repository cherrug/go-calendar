progname=go-calendar

build: clean
	go build -o build/$(progname)

clean:
	rm -rf build

run-prod: build
	./build/$(progname)

run-dev: build
	GCL_DEV=true ./build/$(progname)