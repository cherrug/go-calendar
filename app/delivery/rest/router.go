package rest

import (
	"github.com/gorilla/mux"
	"gitlab.com/cherrug/go-calendar/logger"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func Run(host string, port int, l *logger.Logger) {
	var b strings.Builder
	b.WriteString(":")
	b.WriteString(strconv.Itoa(port))

	router := mux.NewRouter()

	router.HandleFunc("/", getHome).Methods("GET")

	l.Zap.Infof("listening on %s:%d", host, port)

	log.Fatal(http.ListenAndServe(b.String(), router))
}
