package repository

type User struct {
	ID      int
	Email   string
	IsAdmin bool
}

type UserCRUD interface {
	Create(user User) error
	GetByID(id int) (User, error)
	Update(u *User) error
	Delete(id int) error
}

type UserRepository interface {
	GetAll() ([]*User, error)
	GetByEmail(email string) (User, error)
	UserCRUD
}
