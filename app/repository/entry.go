package repository

import "time"

type Entry struct {
	ID      int
	Title   string
	Body    string
	OwnerID int
	Date    time.Time
}

type EntryRepository interface {
	GetAll() ([]*Entry, error)
	GetByID(id int) (Entry, error)
	Create(e Entry) error
	Update(e *Entry) error
	Delete(id int) error
}

//TODO some repo implementation
