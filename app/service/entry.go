package service

import (
	repo "gitlab.com/cherrug/go-calendar/app/repository"
	"gitlab.com/cherrug/go-calendar/logger"
	"time"
)

type EntryService interface {
	GetByDate(time time.Time, uid int) ([]*repo.Entry, error)
	GetByMonth(month time.Month, uid int) ([]*repo.Entry, error)
	GetByID(id int, uid int) (*repo.Entry, error)
	Create(e repo.Entry, uid int) error
	Update(e *repo.Entry, uid int) error
	Delete(id int, uid int) error
}

type EntryImpl struct {
	UserImpl
	EntryRepo repo.EntryRepository
	Logger    logger.Logger
}

func (ei EntryImpl) Create(e repo.Entry, uid int) error {
	// check if user exists
	u, err := ei.UserImpl.GetByID(uid)
	if err != nil {
		return err
	}

	e.OwnerID = u.ID
	err = ei.EntryRepo.Create(e)
	if err != nil {
		ei.Logger.Zap.Error(err)
		return err
	}
	return nil
}

func (ei EntryImpl) GetByID(u repo.User, id int) (repo.Entry, error) {
	//TODO implement
	return repo.Entry{}, nil
}

func (ei EntryImpl) GetByDate(time time.Time, uid int) ([]*repo.Entry, error) {
	//TODO implement
	return nil, nil
}

func (ei EntryImpl) GetByMonth(month time.Month, uid int) ([]*repo.Entry, error) {
	//TODO implement
	return nil, nil
}

func (ei EntryImpl) Update(e *repo.Entry, uid int) error {
	//TODO implement
	return nil
}

func (ei EntryImpl) Delete(id int, uid int) error {
	//TODO implement
	return nil
}
