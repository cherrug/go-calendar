package service

import (
	"fmt"
	repo "gitlab.com/cherrug/go-calendar/app/repository"
	"gitlab.com/cherrug/go-calendar/logger"
)

type UserService interface {
	GetAll(id int) ([]*repo.User, error)
	repo.UserCRUD
}

type UserImpl struct {
	UserRepo repo.UserRepository
	Logger   logger.Logger
}

func (ui UserImpl) Create(u repo.User) error {
	err := ui.UserRepo.Create(u)
	if err != nil {
		// TODO логировать на уровне возникновения ошибки, т.е. на уровне репозитория
		// так как на уровне репозитория
		ui.Logger.Zap.Error(err)
	}
	return err
}

func (ui UserImpl) GetByID(id int) (repo.User, error) {
	return ui.UserRepo.GetByID(id)
}

func (ui UserImpl) Update(u *repo.User) error {
	//TODO some implementation here
	return nil
}

func (ui UserImpl) Delete(id int) error {
	//TODO some implementation here
	return nil
}

func (ui UserImpl) GetAll(id int) ([]*repo.User, error) {
	u, err := ui.GetByID(id)
	if err != nil {
		ui.Logger.Zap.Error(err)
		return nil, err
	}

	if !u.IsAdmin {
		ui.Logger.Zap.Errorf("%d is not admin", id)
		return nil, fmt.Errorf("forbidden, not admin rights")
	}

	return ui.UserRepo.GetAll()
}
