package logger

import (
	"gitlab.com/cherrug/go-calendar/config"
	"go.uber.org/zap"
	"log"
)

type Logger struct {
	Zap *zap.SugaredLogger
}

func New(cfg config.Cfg) Logger {
	var c zap.Config
	if cfg.Development {
		c = zap.NewDevelopmentConfig()
		c.OutputPaths = []string{"stdout"}
	} else {
		c = zap.NewProductionConfig()
	}
	c.OutputPaths = append(c.OutputPaths, cfg.Log.File)
	l, err := c.Build()
	if err != nil {
		log.Fatalln(err)
	}
	return Logger{Zap: l.Sugar()}
}
