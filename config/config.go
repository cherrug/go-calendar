package config

import (
	"github.com/spf13/viper"
	"log"
)

type Cfg struct {
	Development bool
	Web
	Log
}

type Web struct {
	Port int
	Host string
}

type Log struct {
	File string
}

func New() Cfg {
	var c Cfg
	v := viper.New()
	var file string
	v.SetEnvPrefix("gcl")
	v.AutomaticEnv()

	// if ENV contains GCL_DEV then run in development mode
	if v.GetBool("DEV") {
		// development
		file = "development"
		c.Development = true
	} else {
		// production
		file = "production"
	}

	v.SetConfigName(file)
	v.AddConfigPath("./config")
	err := v.ReadInConfig()
	if err != nil {
		log.Fatalln(err)
	}

	err = v.Unmarshal(&c)
	if err != nil {
		log.Fatalln(err)
	}

	return c
}
